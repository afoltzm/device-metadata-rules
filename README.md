# README #


### What is this repository for? ###

* This contains a basic parser and ruleset for assigning a single canonical set of device metadata with multiple sets of differing metadata.
* Each set of fields is independtly processed: age, gender, location, and device metadata are all parsed separately and then combined to give the most informative view of the data for a given device_id.
* v0.7

### How do I get set up? ###

Clone the repo. It doesn't (yet) have any external data dependencies. The unit tests are hand-picked from actual device metadata.

This isn't in production, so deployment isn't an issue.

### Contribution guidelines ###

* If you find a corner case of improperly parsed device data, submit a pull request to have it added to the unit tests.
* Major changes to the logic of the device parser should be reflected in the graphViz diagram that accompanies this repo.

### Who do I talk to? ###

* Andrew
