package com.sitomobile.devicemetadata

package object functions extends java.io.Serializable {
  /* Returns the most commonly occurring element in the array, None if empty
   *
   * Pure function, avoids non-determinism of groupby(identity).maxby() in spark
   * Runs in 0(n) time
   * source: https://stackoverflow.com/questions/46516650/how-do-i-efficiently-get-n-most-frequent-elements-from-my-collection
   */
  def getMaxFromArray[A](s: Seq[A]): A = {
    s.foldLeft(Map.empty[A, Int]) { (map, A) =>
      val count: Int = map.getOrElse(A, 0)
      map.updated(A, count + 1)
    }.toVector.sortWith(_._2 > _._2)(0)._1
  }
}
