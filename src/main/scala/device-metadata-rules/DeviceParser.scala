package com.sitomobile.devicemetadata

import org.apache.spark.sql.types.{ StructType, StringType, StructField }
import org.apache.spark.sql.functions.{ col, udf }
import org.apache.spark.sql.Row

package object DeviceParser extends java.io.Serializable {

  // the quality of a device is assumed to be "good" until proven otherwise:
  // - "generic"
  // - "conflict"
  final case class DeviceInfo(
    make: Option[String] = None,
    model: Option[String] = None,
    os: Option[String] = None,
    osVersion: Option[String] = None,
    deviceType: Option[String] = None,
    deviceLanguage: Option[String] = None,
    quality: String = "good")

  val badDeviceInfo = DeviceInfo(
    None,
    None,
    None,
    None,
    None,
    None,
    "conflict")

  val genericMakeList = Array(
    "generic",
    "",
    "sdk",
    "android")

  val genericModelList = Array(
    "smartphone",
    "android phone",
    "phone",
    "",
    "sdk")

  val replaceMap = Map("-" -> " ", "+" -> " ")

  /* Returns true if the device metadata is actually meaningful
     * Helper function to identify device conflicts */
  def containsInfo(d: DeviceInfo): Boolean = {
    val make = d.make.getOrElse("")
    val model = d.model.getOrElse("")
    val makeInfo = !genericMakeList.contains(make)
    val modelInfo = (!genericModelList.contains(model) ||
      !model.contains("android"))
    makeInfo || modelInfo
  }
  /** Returns true if any device in the input array is non-generic */
  def containsAllInfo(deviceData: Array[DeviceInfo]): Boolean = {
    deviceData
      .map { dd => containsInfo(dd) }
      .exists { _ == true }
  }

  /**
   * Returns true if a conflicting device in the input array is an outlier
   * Currently, considers the mode the outlier, but this could be changed
   */
  def checkOutlier(deviceData: Array[DeviceInfo]): Boolean = {
    val counts = deviceData.groupBy(identity).mapValues(_.size)
    !counts.values.forall(x => counts.values.forall(_ == x))
  }

  def filterOutliers(deviceData: Array[DeviceInfo]): DeviceInfo = {
    val dataSizes = deviceData.toSet
      .map { x: DeviceInfo => (x, deviceData.count(_ == x)) }.toMap
    dataSizes.maxBy(_._2)._1
  }

  def filterGenerics(deviceData: Array[DeviceInfo]): Array[DeviceInfo] = {
    deviceData.filter(containsInfo(_))
  }

  /**
   * Checks two devices against each other to look for a genuine conflict
   * Returns true if no conflict, false otherwise
   *
   * Note: intended to be used after 'generic' devices have been filtered
   */
  def testDevices(d1: DeviceInfo, d2: DeviceInfo): Boolean = {
    val d1model = d1.model.getOrElse("")
    val d1make = d1.make.getOrElse("")
    val d2model = d2.model.getOrElse("")
    val d2make = d2.make.getOrElse("")

    if (d1 == d2) { true }
    else if (containsChecker(d1, d2)) { true }
    else if (d1make == "generic" || d2make == "generic" ||
      d1model.contains("android") || d2model.contains("android")) { true }
    else if ({

      // common string substitutions
      val d1modelr = d1model.map(c =>
        replaceMap.getOrElse(c.toString, c)).mkString
      val d1maker = d1make.map(c =>
        replaceMap.getOrElse(c.toString, c)).mkString
      val d2modelr = d2model.map(c =>
        replaceMap.getOrElse(c.toString, c)).mkString
      val d2maker = d2make.map(c =>
        replaceMap.getOrElse(c.toString, c)).mkString

      (d1modelr == d2modelr) || d1modelr.contains(d2modelr) ||
        d2modelr.contains(d1modelr)
    }) { true }

    else { false }
  }

  /**
   * Checks all devices in an array against each other by pairwise combos
   * Returns true if no conflict, false otherwise
   */
  def testAllDevices(deviceData: Array[DeviceInfo]): Boolean = {
    if (deviceData.size == 0) { false }
    else {
      deviceData.combinations(2)
        .map { dd => testDevices(dd(0), dd(1)) }
        .forall { _ == true }
    }
  }

  /* Returns true if left's data is the subset of right's
     * Corner case: what about "iphone" vs "iphone 7"?
     */
  def isSubstrDevice(left: DeviceInfo, right: DeviceInfo): Boolean = {
    val lmodel = left.model.getOrElse("")
    val lmake = left.make.getOrElse("")
    val rmodel = right.model.getOrElse("")
    val rmake = right.make.getOrElse("")
    // pulling values out of "some" for direct comparison

    ((lmodel != rmodel) && (lmake != rmake)) && // ensuring strict subset
      (rmodel.contains(lmake)
        //looking for "lg" in "lgls996"
        || rmake.contains(lmake))
    // looking for "lg" in "lge"
  }

  def containsChecker(left: DeviceInfo, right: DeviceInfo): Boolean = {
    val lmodel = left.model.getOrElse("")
    val lmake = left.make.getOrElse("")
    val rmodel = right.model.getOrElse("")
    val rmake = right.make.getOrElse("")
    ((rmodel.contains(lmake) || lmodel.contains(rmake)) ||
      (rmodel.contains(lmodel) || lmodel.contains(rmodel)) ||
      ((rmake != lmake) &&
        (rmake.contains(lmake) || lmake.contains(rmake))))
  }

  /* Returns true if the device isn't a subset of any other in the list
     */
  def notSubstrOfOthers(d1: DeviceInfo, d2: Array[DeviceInfo]): Boolean = {
    d2.forall(!isSubstrDevice(_, d1))
  }

  /* Removes any device model with 1 metadata field that's a subset of another
     *
     */
  def filterSubstrDevice(deviceData: Array[DeviceInfo]): Array[DeviceInfo] = {
    val uniques = deviceData.toSet.toArray
    uniques.filter { x =>
      notSubstrOfOthers(x, uniques.filter(_ != x))
    }
  }

  /* Returns the most meaningful device from pre-filtered list of device data
     */
  def bestDeviceChooser(deviceData: Array[DeviceInfo]): DeviceInfo = {
    val filteredDeviceData = deviceData.toSet.toArray
      .filter { d =>
        !genericModelList.contains(d.model.getOrElse("")) &&
          !genericMakeList.contains(d.make.getOrElse(""))
      }
    // lose remaining generics and nulls
    if (filteredDeviceData.map(_.make.getOrElse("")).contains("apple")) {
      return filteredDeviceData.maxBy { d =>
        deviceInfoToArray(d: DeviceInfo).map(_.size).reduce(_ + _)
      }
    } // if apple, get the most complete data
    val noSubstrs = filterSubstrDevice(filteredDeviceData)
    //assert(noSubstrs.size > 0)
    if (noSubstrs.size > 0) {
      noSubstrs.minBy(_.model.getOrElse("").size)
    } // return the least verbose
    else { deviceData.maxBy(_.model.getOrElse("").size) }
    // if only generics remain, then we go with the longest one
  }

  /* Returns the best candidate for a device ID's metadata
     */
  def testDevicesSet(data: Array[DeviceInfo]): DeviceInfo = {
    if (data.size == 0) { return badDeviceInfo }
    if (data.distinct.size == 1) { return data(0) }
    val nonGeneric = containsAllInfo(data)
    val dataNoGeneric = filterGenerics(data)

    if (!nonGeneric) { data(0) }
    else {
      if (testAllDevices(dataNoGeneric)) {
        bestDeviceChooser(dataNoGeneric)
      } else {
        if (checkOutlier(dataNoGeneric)) {
          filterOutliers(dataNoGeneric)
        } else {
          badDeviceInfo
        }
      }
    }
  }

  def deviceInfoToArray(d: DeviceInfo): Array[String] = {
    Array(
      d.make.getOrElse(""),
      d.model.getOrElse(""),
      d.os.getOrElse(""),
      d.osVersion.getOrElse(""),
      d.deviceType.getOrElse(""),
      d.deviceLanguage.getOrElse(""),
      d.quality)
  }

  def deviceDataToParsed(dInfo: Seq[Seq[String]]): Array[String] = {
    val dInfoArray = dInfo.toArray
    val dInfoParsed = dInfoArray.map { x =>
      DeviceInfo(
        Option(x(0)),
        Option(x(1)),
        Option(x(2)),
        Option(x(3)),
        Option(x(4)),
        Option(x(5)),
        "good")
    }
    deviceInfoToArray(
      testDevicesSet(dInfoParsed))
  }

  def parseUdf = udf(deviceDataToParsed _)

  def parseDevicesAndTest(deviceData: Seq[Row]): Array[String] = {
    val dInfo = deviceData.toArray.map { x =>
      DeviceInfo(
        Option(x.getString(0)),
        Option(x.getString(1)),
        Option(x.getString(2)),
        Option(x.getString(3)),
        Option(x.getString(4)),
        Option(x.getString(5)),
        "good")
    }
    val parsed = testDevicesSet(dInfo)
    deviceInfoToArray(parsed)
  }

  val groupUdf = udf((x: Seq[Row]) =>
    parseDevicesAndTest(x))
}
