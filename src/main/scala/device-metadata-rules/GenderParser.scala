package com.sitomobile.devicemetadata

import scala.collection.mutable.WrappedArray
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions.{ collect_list, col, udf }

package object genderparser extends java.io.Serializable {

  def aggFilter(genderList: WrappedArray[String]): String = {
    val filteredList = genderList.filter { i =>
      i == "m" || i == "f" || i == "o"
    }
    if (filteredList.size == 0) { "N/A" }
    else {
      filteredList
        .groupBy(identity)
        .maxBy(_._2.size)._1
    }
  }
  val aggFilterUdf = udf(aggFilter _)

  def getDeviceGender(input: DataFrame): DataFrame = {
    val grouped = input.select("device_id", "gender")
      .groupBy("device_id")
      .agg(collect_list(col("gender")).as("gender"))

    grouped.withColumn("parsedGender", aggFilterUdf(col("gender")))
  }

}
