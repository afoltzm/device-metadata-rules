package com.sitomobile.devicemetadata

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions.{ udf, sum, max, col }
import scala.collection.mutable.WrappedArray

package object ageparser extends java.io.Serializable {
  def processAge(age: Option[Int]): Option[Int] = {
    age.getOrElse(0) match {
      case it if 16 until 120 contains it => Some(it)
      case it if 1898 until 2002 contains it => Some(2018 - it)
      case it if it > 2003 => None
      case it if it <= 0 => None
      case _ => None
    }
  }

  def getNumString(a: String): Option[Int] = {
    try { Some(a.toInt) }
    catch {
      case badFormat: java.lang.NumberFormatException => { None }
    }
  }

  def processAges(deviceAges: WrappedArray[Option[Int]]): Option[Int] = {
    val agesProcessed = deviceAges.map(processAge(_)) //.filter(_ != None)
    if (agesProcessed.size == 0) { None }
    else { functions.getMaxFromArray(agesProcessed.toSeq) }
  }

  val processUDF = udf((x: WrappedArray[Option[Int]]) => processAges(x))
}

package object ageparser_string extends java.io.Serializable {
  def getNumString(a: String): Option[Int] = {
    try { Some(a.toInt) }
    catch {
      case badFormat: java.lang.NumberFormatException => { None }
    }
  }

  val processUDF = udf((x: WrappedArray[String]) =>
    ageparser.processAges(x.map(getNumString(_))))
}

