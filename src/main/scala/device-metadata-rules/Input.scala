package com.sitomobile.devicemetadata

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions.{ collect_list, struct }
import java.time.LocalDate
import org.apache.spark.sql.types._
import scala.collection.mutable.ArrayBuffer

package object input extends java.io.Serializable {

  val bidReqSchema = StructType(Array(
    StructField("device_id", StringType),
    StructField("count", LongType),
    StructField("exchange", StringType),
    StructField("utc_datetime", TimestampType),
    StructField("est_datetime", TimestampType),
    StructField("os", StringType),
    StructField("make", StringType),
    StructField("model", StringType),
    StructField("carrier", StringType),
    StructField("gender", StringType),
    StructField("age", StringType),
    StructField("ip", StringType),
    StructField("media_type", StringType),
    StructField("ad_size", StringType),
    StructField("connection_type", StringType),
    StructField("device_type", StringType),
    StructField("browser_user_agent", StringType),
    StructField("os_version", StringType),
    StructField("hardware_version", StringType),
    StructField("js", LongType),
    StructField("language", StringType),
    StructField("geohash10", StringType),
    StructField("city", StringType),
    StructField("country", StringType),
    StructField("region", StringType),
    StructField("zip", StringType),
    StructField("geo_type", StringType),
    StructField("is_aws", BooleanType),
    StructField("is_google", StringType),
    StructField("is_msft", StringType),
    StructField("is_whiteops", StringType),
    StructField("timezone", StringType),
    StructField("local_datetime", TimestampType),
    StructField("hour", LongType)))

  /* Gets the paths of the the day-partitioned input data from the input path
     * Based on a a specified date range
     * Expects a base path without "year=x" filled in
     */
  def getDataPaths(
    basePath: String,
    days: Long = 90,
    end: LocalDate = LocalDate.now): Array[String] = {
    val startDate = end.minusDays(days)
    val range = getDatesInRange(startDate, end)
    val paths = range.map { x =>
      val year = x.getYear
      val month = x.getMonthValue
      val day = x.getDayOfMonth
      basePath + "year=" + x.getYear + "/month=" + x.getMonthValue + "/day=" +
        x.getDayOfMonth
    }
    paths
  }

  /* A utility function to get LocalDates in a range
     */
  def getDatesInRange(start: LocalDate, end: LocalDate): Array[LocalDate] = {
    var mutableDate = start
    var rng = ArrayBuffer[LocalDate]()
    while (mutableDate.isBefore(end.plusDays(1))) {
      rng = rng :+ mutableDate
      mutableDate = mutableDate.plusDays(1)
    }
    rng.toArray
  }

  def selectInputExpr(inputData: DataFrame): DataFrame = {
    inputData
      .selectExpr(
        "lower(device_id) AS device_id",
        "count",
        "exchange",
        "from_utc_timestamp(utc_datetime, 'GMT') AS utc_datetime",
        // ensuring that the time doesn't get cast to local machine time
        "est_datetime",
        "lower(os) AS os",
        "lower(make) AS make",
        "lower(model) AS model",
        "lower(carrier) AS carrier",
        "lower(gender) AS gender",
        "lower(age) AS age",
        "ip",
        "lower(media_type) AS media_type",
        "lower(ad_size) AS ad_size",
        "lower(connection_type) AS connection_type",
        "lower(device_type) AS device_type",
        "browser_user_agent",
        "lower(os_version) AS os_version",
        "lower(hardware_version) AS hardware_version",
        "js",
        "lower(language) AS language",
        "geohash10",
        "city",
        "country",
        "region",
        "zip",
        "geo_type",
        "is_aws",
        "is_google",
        "is_msft",
        "is_whiteops",
        "timezone",
        "local_datetime",
        "hour")
  }
}

/* V1 of the deviceMatrix code needs to be substantially faster.
   * This means that we need to pregroup the device data into lists of values
   * and structs that the logic can operate on. This way, we avoid an extremely
   * expensive shuffle with each separate operation.
   */
object InputGrouped extends java.io.Serializable {

  def groupInput(inputData: DataFrame): DataFrame = {


    val inputToGroup = inputData
      .withColumn(
        "device_metadata",
        struct(
          "make",
          "model",
          "os",
          "os_version",
          "device_type",
          "language"))
      .withColumn(
        "location_data",
        struct(
          "city",
          "zip"))
      .select(
        "device_id",
        "device_metadata",
        "location_data",
        "age",
        "gender",
        "carrier",
        "utc_datetime")


    inputToGroup
      .groupBy("device_id")
      .agg(
        collect_list("device_metadata").alias("device_metadata"),
        collect_list("location_data").alias("location_data"),
        collect_list("age").alias("age"),
        collect_list("gender").alias("gender"),
        collect_list("carrier").alias("carrier"),
        collect_list("utc_datetime").alias("utc_datetime"))
  }
}
