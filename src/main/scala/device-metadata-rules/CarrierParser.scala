package com.sitomobile.devicemetadata

import scala.collection.mutable.WrappedArray
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions.{ collect_list, col, udf }

package object carrierparser extends java.io.Serializable {
  def processCarrier(carrierList: WrappedArray[String]): String = {
    val filteredList = carrierList.filter { i =>
      i != null && i != "wifi" && i != "unknown - probably wlan" &&
        i != "wlan" && i != "-" && i != "home" && i != "" &&
        !i.contains("wifi") && !i.contains("wlan") && !i.contains("unknown")
    }
    if (filteredList.size == 0) { "N/A" }
    else {
      filteredList
        .groupBy(identity)
        .maxBy(_._2.size)._1
    }
  }

  val processUDF = udf(processCarrier _)
}
