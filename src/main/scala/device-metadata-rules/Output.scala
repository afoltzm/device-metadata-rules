package com.sitomobile.devicemetadata

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions.col

package object output {
  /**
   * Returns the parsed device matrix data from a pre-grouped input dataframe
   */
  def getOutput(inputDF: DataFrame): DataFrame = {

    val inputWithMetadata = inputDF.withColumn(
      "metadata_parsed", DeviceParser.groupUdf(col("device_metadata")))
    val inputWithLocation = inputWithMetadata.withColumn(
      "location_parsed", locationparser.processUDF(col("location_data")))
    val inputWithAge = inputWithLocation.withColumn(
      "age_parsed", ageparser_string.processUDF(col("age")))
    val inputWithGender = inputWithAge.withColumn(
      "gender_parsed", genderparser.aggFilterUdf(col("gender")))
    val inputWithCarrier = inputWithGender.withColumn(
      "carrier_parsed", carrierparser.processUDF(col("carrier")))
    val result = inputWithCarrier.withColumn(
      "last_seen_parsed", lastseenparser.processUDF(col("utc_datetime")))
    result.selectExpr(
      "device_id",
      "metadata_parsed[0] AS device_make",
      "metadata_parsed[1] AS device_model",
      "metadata_parsed[2] AS device_os",
      "metadata_parsed[3] AS device_os_version",
      "metadata_parsed[4] AS device_type",
      "metadata_parsed[5] AS device_language",
      "metadata_parsed[6] AS device_data_quality",
      "location_parsed[0] AS city",
      "location_parsed[1] AS zip",
      "age_parsed AS age",
      "gender_parsed AS gender",
      "carrier_parsed AS carrier",
      "last_seen_parsed AS last_seen")
  }
}
