package com.sitomobile.devicemetadata

import org.apache.spark.sql.{ DataFrame, Row }
import org.apache.spark.sql.functions.{ col, sum, desc, udf, row_number }

package object locationparser extends java.io.Serializable {

  def processLocations(locations: Seq[Row]): Array[String] = {
    val sep = "-_-"
    val locationsArray = locations.map { r: Row =>
      r.getString(0) + sep + r.getString(1)
    }
    val maxLoc = if (locationsArray.size == 0) { "N/A" + sep + "N/A" }
    else { functions.getMaxFromArray(locationsArray) }
    maxLoc.split(sep)
  }

  val processUDF = udf((x: Seq[Row]) => processLocations(x))
}

