package com.sitomobile.devicemetadata

import java.sql.Timestamp
import scala.collection.mutable.WrappedArray
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions.{ col, max, udf }

package object lastseenparser extends java.io.Serializable {
  // def getLastSeen(input: DataFrame): DataFrame = {
  //   input
  //     .filter(col("utc_datetime").isNotNull)
  //     .groupBy("device_id")
  //     .agg(max(col("utc_datetime")).as("last_seen"))
  // }

  def processLastSeen(times: WrappedArray[Timestamp]): Timestamp = {
    val sortedTimes = times.sortWith((t1, t2) => t1.after(t2))
    sortedTimes(0)
  }

  val processUDF = udf(processLastSeen _)
}
