package com.sitomobile.devicemetadata

import java.sql.Timestamp
import scala.io.Source
import scala.collection.mutable.WrappedArray
import com.sitomobile.devicemetadata._
import org.scalatest.FunSpec
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions._
import com.github.mrpowers.spark.fast.tests.DataFrameComparer
import org.apache.spark.sql.types._
import org.apache.spark.sql.Row
import java.time.LocalTime

// main test object

class ParserTest
  extends FunSpec
  with SparkSessionTestWrapper
  with DataFrameComparer {

  import spark.implicits._

  describe("Parsed Device MetaData") {

    val badDeviceInfo = DeviceParser.DeviceInfo(
      None,
      None,
      None,
      None,
      None,
      None,
      "conflict")

    val lgTestData = DeviceParser.DeviceInfo(
      Option("lg"), Option("k210"))
    val ssTestData = DeviceParser.DeviceInfo(
      Option("samsung"), Option("sm-270"))
    val ssTestData2 = DeviceParser.DeviceInfo(
      Option("samsung"), Option("sm 270"))
    val ssTestData3 = DeviceParser.DeviceInfo(
      Option("samsung"), Option("android 6.0"))
    val genericAndroidTestData = DeviceParser.DeviceInfo(
      Option("generic"), Option("android phone"))

    val goodSetTestData = Array(lgTestData, genericAndroidTestData) // no conflict
    val conflictSetTestData = Array(
      genericAndroidTestData,
      DeviceParser.DeviceInfo(Option("samsung"), Option("sm-j727t")),
      DeviceParser.DeviceInfo(Option("lg"), Option("m154"))
    // two different makes is a pretty obvious flag
    )

    it("should detect generic data") {
      assert(DeviceParser.containsInfo(genericAndroidTestData) == false)
    }

    it("should detect conflicts between devices") {
      assert(DeviceParser.testDevicesSet(conflictSetTestData) ==
        badDeviceInfo)
    }

    val real01 = Array(
      DeviceParser.DeviceInfo(Option("generic"), Option("android phone")),
      DeviceParser.DeviceInfo(Option("lge"), Option("lgls996")),
      DeviceParser.DeviceInfo(Option("lg"), Option("ls996")),
      DeviceParser.DeviceInfo(Option("generic"), Option("ls996")),
      DeviceParser.DeviceInfo(Option("generic"), Option("android phone")))
    val real01_expected = DeviceParser.DeviceInfo(
      Option("lg"),
      Option("ls996"))

    val real02 = Array(
      DeviceParser.DeviceInfo(Option("samsung"), Option("sm-g955u")),
      DeviceParser.DeviceInfo(Option("zte"), Option("n9560")),
      DeviceParser.DeviceInfo(Option("generic"), Option("android 6.0")),
      DeviceParser.DeviceInfo(Option("samsung"), Option("sm-g891a")),
      DeviceParser.DeviceInfo(Option("generic"), Option("android phone")),
      DeviceParser.DeviceInfo(Option("lg"), Option("ms210")),
      DeviceParser.DeviceInfo(Option("zte"), Option("z8181")),
      DeviceParser.DeviceInfo(Option("samsung"), Option("sm-n950u")),
      DeviceParser.DeviceInfo(Option("samsung"), Option("sch-1710")))
    val real02_expected = badDeviceInfo

    //2 actual devices that were interpreted correcly by the first ruleset
    val real03 = Array(
      DeviceParser.DeviceInfo(Option("zte"), Option("n9560")),
      DeviceParser.DeviceInfo(Option("generic"), Option("android phone")),
      DeviceParser.DeviceInfo(Option("generic"), Option("android")))

    val real03_expected = DeviceParser.DeviceInfo(
      Option("zte"), Option("n9560"))

    val real04 = Array(
      DeviceParser.DeviceInfo(Option("samsung"), Option("sm-n920v")),
      DeviceParser.DeviceInfo(Option("generic"), Option("android 2.0")),
      DeviceParser.DeviceInfo(Option("generic"), Option("android phone")),
      DeviceParser.DeviceInfo(Option("generic"), Option("sm-n920v")))
    val real04_expected = DeviceParser.DeviceInfo(
      Option("samsung"), Option("sm-n920v"))

    val real05 = Array(
      DeviceParser.DeviceInfo(Option("lg"), Option("lgmp260")),
      DeviceParser.DeviceInfo(Option("lge"), Option("lgmp260")),
      DeviceParser.DeviceInfo(Option("generic"), Option("android 2.0")),
      DeviceParser.DeviceInfo(Option("generic"), Option("android phone")),
      DeviceParser.DeviceInfo(Option("generic"), Option("android")),
      DeviceParser.DeviceInfo(Option("lg"), Option("mp260")),
      DeviceParser.DeviceInfo(Option("generic"), Option("mp260")),
      DeviceParser.DeviceInfo(Option("lg"), Option("lgmp260")))
    val real05_expected = DeviceParser.DeviceInfo(
      Option("lg"), Option("mp260"))

    val real06 = Array(
      DeviceParser.DeviceInfo(Option("motorola"), Option("xt1684")),
      DeviceParser.DeviceInfo(Option("motorola"), Option("moto g (5) plus")))
    val real06_expected = badDeviceInfo

    val real07 = Array(
      DeviceParser.DeviceInfo(Option("samsung"), Option("sm-j327p")),
      DeviceParser.DeviceInfo(Option("samsung"), Option("sm-j327p")),
      DeviceParser.DeviceInfo(Option("samsung"), Option("sm-j327p")),
      DeviceParser.DeviceInfo(Option("samsung"), Option("sm-j327p")),
      DeviceParser.DeviceInfo(Option("samsung"), Option("sm-j327p")),
      DeviceParser.DeviceInfo(Option("samsung"), Option("sm-j327p")),
      DeviceParser.DeviceInfo(Option("samsung"), Option("sm-j327p")),
      DeviceParser.DeviceInfo(Option("generic"), Option("sm-j327p")),
      DeviceParser.DeviceInfo(Option("generic"), Option("sm-j327p")),
      DeviceParser.DeviceInfo(Option("samsung"), Option("gt-p5110")),
      DeviceParser.DeviceInfo(Option("samsung"), Option("gt-p5110")),
      DeviceParser.DeviceInfo(Option("samsung"), Option("gt-p5110")),
      DeviceParser.DeviceInfo(Option("samsung"), Option("gt-p5110")),
      DeviceParser.DeviceInfo(Option("samsung"), Option("gt-p5110")),
      DeviceParser.DeviceInfo(Option("samsung"), Option("gt-p5110")),
      DeviceParser.DeviceInfo(Option("generic"), Option("android 6.0")),
      DeviceParser.DeviceInfo(Option("generic"), Option("android 6.0")),
      DeviceParser.DeviceInfo(Option("generic"), Option("android 6.0")),
      DeviceParser.DeviceInfo(Option("generic"), Option("android 6.0")),
      DeviceParser.DeviceInfo(Option("generic"), Option("android 6.0")),
      DeviceParser.DeviceInfo(Option("generic"), Option("android 6.0")),
      DeviceParser.DeviceInfo(Option("generic"), Option("android phone")),
      DeviceParser.DeviceInfo(Option("generic"), Option("android phone")),
      DeviceParser.DeviceInfo(Option("generic"), Option("android phone")),
      DeviceParser.DeviceInfo(Option("generic"), Option("android phone")),
      DeviceParser.DeviceInfo(Option("generic"), Option("android phone")),
      DeviceParser.DeviceInfo(Option("generic"), Option("android phone")),
      DeviceParser.DeviceInfo(Option("generic"), Option("android phone")),
      DeviceParser.DeviceInfo(Option("generic"), Option("android phone")),
      DeviceParser.DeviceInfo(Option("generic"), Option("android phone")),
      DeviceParser.DeviceInfo(Option("generic"), Option("android phone")),
      DeviceParser.DeviceInfo(Option("generic"), Option("android phone")),
      DeviceParser.DeviceInfo(Option("generic"), Option("android phone")),
      DeviceParser.DeviceInfo(Option("generic"), Option("android phone")),
      DeviceParser.DeviceInfo(Option("generic"), Option("android phone")),
      DeviceParser.DeviceInfo(Option("generic"), Option("android phone")),
      DeviceParser.DeviceInfo(Option("generic"), Option("android phone")),
      DeviceParser.DeviceInfo(Option("generic"), Option("android phone")),
      DeviceParser.DeviceInfo(Option("generic"), Option("android phone")),
      DeviceParser.DeviceInfo(Option("generic"), Option("android phone")),
      DeviceParser.DeviceInfo(Option("generic"), Option("android phone")),
      DeviceParser.DeviceInfo(Option("generic"), Option("android phone")),
      DeviceParser.DeviceInfo(Option("generic"), Option("android phone")),
      DeviceParser.DeviceInfo(Option("generic"), Option("android phone")),
      DeviceParser.DeviceInfo(Option("samsung"), Option("sm-j320n0")),
      DeviceParser.DeviceInfo(Option("generic"), Option("android")),
      DeviceParser.DeviceInfo(Option("generic"), Option("android")))
    val real07_expected = DeviceParser.DeviceInfo(
      Option("samsung"), Option("sm-j327p"))

    val real08 = Array(
      DeviceParser.DeviceInfo(
        Option("apple"), Option("iphone"), Option("mobile/tablet"),
        Option("ios"), Option("11.2")),
      DeviceParser.DeviceInfo(Option("apple"), Option("iphone 7 plus"),
        Option("mobile/tablet"), Option("ios"), Option("11.2")),
      DeviceParser.DeviceInfo(Option("apple"), Option("iphone"),
        Option("mobile/tablet"), Option("ios"), Option("11.2")),
      DeviceParser.DeviceInfo(Option("apple"), Option("iphone"),
        Option("phone"), Option("ios"), Option("11")),
      DeviceParser.DeviceInfo(Option("apple"), Option("iphone"),
        Option("phone"), Option("ios"), Option("11")),
      DeviceParser.DeviceInfo(Option("apple"), Option("iphone"),
        Option("mobile/tablet"), Option("ios"), Option("11.2.1")),
      DeviceParser.DeviceInfo(Option("apple"), Option("iphone 7 plus"),
        Option("mobile/tablet"), Option("ios"), Option("11.2")),
      DeviceParser.DeviceInfo(Option("apple"), Option("iphone"),
        Option("mobile/tablet"), Option("ios"), Option("11.2")),
      DeviceParser.DeviceInfo(Option("apple"), Option("iphone"),
        Option("unknown"), Option(null), Option("11.2")),
      DeviceParser.DeviceInfo(Option("apple"), Option("iphone"),
        Option("phone"), Option("ios"), Option("11")),
      DeviceParser.DeviceInfo(Option("apple"), Option("iphone"),
        Option("phone"), Option("ios"), Option("11")),
      DeviceParser.DeviceInfo(Option("apple"), Option("iphone"),
        Option("phone"), Option("ios"), Option("11")),
      DeviceParser.DeviceInfo(Option("apple"), Option("iphone"),
        Option("mobile/tablet"), Option("ios"), Option("11.2.1")),
      DeviceParser.DeviceInfo(Option("apple"), Option("iphone"),
        Option("mobile/tablet"), Option("ios"), Option("11.2.1")),
      DeviceParser.DeviceInfo(Option("apple"), Option("iphone"),
        Option("phone"), Option("ios"), Option("11.2.1")),
      DeviceParser.DeviceInfo(Option("apple"), Option("iphone"),
        Option("phone"), Option("ios"), Option("11")),
      DeviceParser.DeviceInfo(Option("apple"), Option("iphone+7+plus"),
        Option("unknown"), Option(null), Option("11.2")),
      DeviceParser.DeviceInfo(Option("apple"), Option("iphone"),
        Option("phone"), Option("ios"), Option("11_2_1")))

    val real08_expected = DeviceParser.DeviceInfo(
      Option("apple"), Option("iphone 7 plus"), Option("mobile/tablet"),
      Option("ios"), Option("11.2"))

    it("should parse real-world data correctly") {
      assert(DeviceParser.testDevicesSet(real01) == real01_expected)
      assert(DeviceParser.testDevicesSet(real02) == real02_expected)
      assert(DeviceParser.testDevicesSet(real03) == real03_expected)
      assert(DeviceParser.testDevicesSet(real04) == real04_expected)
      assert(DeviceParser.testDevicesSet(real05) == real05_expected)
      assert(DeviceParser.testDevicesSet(real06) == real06_expected)
      assert(DeviceParser.testDevicesSet(real07) == real07_expected)
      assert(DeviceParser.testDevicesSet(real08) == real08_expected)

    }

    val testCarrierList01 = Array("wifi", null, "unknown - probably wlan", "-",
      "unknown probably wlan").to[collection.mutable.WrappedArray]

    val testCarrierList02 = Array(
      "verizon",
      "unknown - probably wlan", "-").to[collection.mutable.WrappedArray]

    it("should filter out bad carrier data") {
      assert(carrierparser.processCarrier(testCarrierList01) == "N/A")
      assert(carrierparser.processCarrier(testCarrierList02) == "verizon")
    }

    val testAge01 = Some(2015)
    val testAge02 = Some(1967)
    val testAge03 = Some(-1)
    val testAge04 = Some(22)

    it("should parse ages correctly") {
      assert(ageparser.processAge(testAge01) == None)
      assert(ageparser.processAge(testAge02) == Some(51))
      assert(ageparser.processAge(testAge03) == None)
      assert(ageparser.processAge(testAge04) == Some(22))
      assert(ageparser.processAge(Some(0)) == None)

    }
    val device01DF = input.selectInputExpr(local.getTestData("device01.csv"))
    val grouped01DF = InputGrouped.groupInput(device01DF)
    val grouped01data = grouped01DF.collect()(0)

    it("should group a single device's data into an array") {

      assert(grouped01DF.count == 1)
      assert(grouped01data(1).asInstanceOf[WrappedArray[Row]].size == 13)
    }
    /*
    it("should cast the input columns as the correct types") {
      assert(device01DF.schema(10).dataType == IntegerType)
      assert(grouped01DF.schema(3).dataType == ArrayType(IntegerType, true))
    }
     */

    it("should return the most recent timestamp") {
      val timesToSort = Array(
        Timestamp.valueOf("2018-03-24 12:00:00"),
        Timestamp.valueOf("2018-03-26 12:00:00")): WrappedArray[Timestamp]
      assert(lastseenparser.processLastSeen(timesToSort) ==
        timesToSort(1))
    }
    it("should parse the device's data correctly") {
      val expectedMetadata01 = Array(
        "lge", "lgms550", "android", "6.0.1", "unknown", "en", "good")
      val expectedAge01 = null
      val expectedLocation01 = Array("Hollywood", "33025")
      val expectedCarrier01 = "310-260"
      val expectedLastSeen01 = Timestamp.valueOf("2018-03-28 19:00:00")
      //2018-03-28T23:00:00.000-04:00
      val allParsed01DF = output.getOutput(grouped01DF.coalesce(1))

      allParsed01DF.write
        .option("header", "true")
        .mode("overwrite")
        .csv("/home/afm/Desktop/allparsed" + LocalTime.now.toString + ".csv")

      val allParsed01Data = allParsed01DF.collect()(0)

      assert(allParsed01Data(1) == expectedMetadata01(0))
      assert(allParsed01Data(2) == expectedMetadata01(1))
      assert(allParsed01Data(3) == expectedMetadata01(2))
      assert(allParsed01Data(4) == expectedMetadata01(3))
      assert(allParsed01Data(5) == expectedMetadata01(4))
      assert(allParsed01Data(6) == expectedMetadata01(5))
      assert(allParsed01Data(7) == expectedMetadata01(6))
      assert(allParsed01Data(10) == expectedAge01)
      assert(allParsed01Data(8) == expectedLocation01(0))
      assert(allParsed01Data(9) == expectedLocation01(1))
      assert(allParsed01Data(12) == expectedCarrier01)
      // assert(allParsed01Data(13) == expectedLastSeen01)
    }
  }
}
