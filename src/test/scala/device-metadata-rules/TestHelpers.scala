package com.sitomobile.devicemetadata

import java.sql.Timestamp
import scala.io.Source
import scala.collection.mutable.WrappedArray
import com.sitomobile.devicemetadata._
import org.scalatest.FunSpec
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
import org.apache.spark.sql.Row

package object local extends SparkSessionTestWrapper {
  // a helper object for local data reads

  val sqlContext = spark.sqlContext
  val sparkContext = spark.sparkContext

  def getTestData(sourceName: String): DataFrame = {
    spark.read
      .option("header", "true")
      //.schema(input.bidReqSchema) - oddly, specifying the schema yields nullsf
      .option("mode", "FAILFAST")
      .option("delimiter", ",")
      .format("org.apache.spark.csv")
      .csv(
        getClass.getClassLoader.getResource(sourceName).getPath)
  }

}

class LocalInputTest
  extends FunSpec
  with SparkSessionTestWrapper {

  it("should read in the data correctly") {
    val device01DF = local.getTestData("device01.csv")

    assert(device01DF.take(1)(0).getString(0) == "01a")

  }
}
